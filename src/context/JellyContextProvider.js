import React, { useState } from 'react'

const JellyContext = React.createContext([{}, () => {}])

const JellyContextProvider = props => {
  const [jellyVisibility, setJellyVisibility] = useState(true)
  return (
    <JellyContext.Provider value={[jellyVisibility, setJellyVisibility]}>
      {props.children}
    </JellyContext.Provider>
  )
}

export { JellyContext, JellyContextProvider }
