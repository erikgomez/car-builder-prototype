import React from 'react'

// TODO: This should become a request to the server mock data
import bapConfig from '../data/bap_config'

const BAPConfigContext = React.createContext(bapConfig)

export default BAPConfigContext
