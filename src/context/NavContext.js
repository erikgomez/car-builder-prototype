import React, { useState } from 'react'
import BAPConfigContext from './BAPConfigContext'

// create and initialize the nav context
const NavContext = React.createContext([{}, () => {}])

/* 
TODO: 
    - This might need to become a typed component
    - Also need to look into performance issues due to 
    getNavStructures being called on every use of this context.
 */

const NavContextProvider = props => {
  const navStructures = getNavStructures(React.useContext(BAPConfigContext))
  const [navState, setNavState] = useState({
    curNavIndex: 0,
    navLinks: navStructures.navLinks,
    navMap: navStructures.navMap,
  })
  return (
    <NavContext.Provider value={[navState, setNavState]}>
      {props.children}
    </NavContext.Provider>
  )
}

const getNavStructures = data => {
  let { navLinks, navMap } = data.categories.reduce(
    (acc, category) => {
      if (
        category.subCategories.length === 1 &&
        category.subCategories[0].title === ''
      ) {
        acc.navLinks.push(category.title)
        acc.navMap[category.title] = acc.navLinks.length - 1
        return acc
      } else {
        category.subCategories.map(subCat => {
          acc.navLinks.push(`${category.title}/${subCat.title}`)
          if (!acc.navMap[category.title]) {
            acc.navMap[category.title] = acc.navLinks.length - 1
          }
          acc.navMap[`${category.title}/${subCat.title}`] =
            acc.navLinks.length - 1
          return acc
        })
        return acc
      }
    },
    { navLinks: [], navMap: {} }
  )
  navLinks.push('summary')
  navMap['summary'] = navLinks.length - 1

  //   console.group('getNavStructures')
  //   console.log('navLinks', navLinks)
  //   console.log('navMap', navMap)
  //   console.groupEnd()
  return { navLinks, navMap }
}

export { NavContext, NavContextProvider }
