const trimGroupConfig = {
  categories: [
    {
      title: 'powertrain',
      subCategories: [
        {
          title: '',
          options: [
            {
              title: '6-Speed Manual',
              optionDescription: 'w/2.0L 4-Cyl. Engine',
              price: 0,
            },
            {
              title: 'CVT',
              optionDescription: 'w/2.0L 4-Cyl. Engine',
              price: 800,
            },
            {
              title: '10-Speed Automatic',
              optionDescription: 'w/2.0L 4-Cyl. Engine',
              price: 1110,
            },
          ],
        },
      ],
    },
    {
      title: 'appearance',
      subCategories: [
        {
          title: 'exterior',
          options: [
            {
              id: 1,
              label: 'exterior',
              title: 'Color Ext Name 1',
            },
            {
              id: 2,
              label: 'exterior',
              title: 'Color Ext Name 2',
            },
            {
              id: 3,
              label: 'exterior',
              title: 'Color Ext Name 3',
            },
            {
              id: 4,
              label: 'exterior',
              title: 'Color Ext Name 4',
            },
            {
              id: 5,
              label: 'exterior',
              title: 'Color Ext Name 5',
            },
          ],
        },
        {
          title: 'interior',
          options: [
            {
              id: 1,
              label: 'interior',
              title: 'Color Int Name 1',
            },
            {
              id: 2,
              label: 'interior',
              title: 'Color Int Name 2',
            },
            {
              id: 3,
              label: 'interior',
              title: 'Color Int Name 3',
            },
            {
              id: 4,
              label: 'interior',
              title: 'Color Int Name 4',
            },
            {
              id: 5,
              label: 'interior',
              title: 'Color Int Name 5',
            },
          ],
        },
        {
          title: 'wheels',
          options: [
            {
              id: 1,
              label: 'wheels',
              title: '16-inch Wheels with Covers',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-trim-modal-lx-standard-wheels-320-1x.jpg',
            },
            {
              id: 2,
              label: 'wheels',
              title: '17-Inch Black Alloy Wheels w/ Tires',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-17in-black-wheel-thumb-320-1x.jpg',
            },
            {
              id: 3,
              label: 'wheels',
              title: '17-Inch Machine-Finished Alloy Wheels w/Tires',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-17in-machine-finished-black-wheel-thumb-320-1x.jpg',
            },
          ],
        },
        {
          title: 'rockets!',
          options: [
            {
              id: 1,
              label: 'wheels',
              title: '16-inch Wheels with Covers',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-trim-modal-lx-standard-wheels-320-1x.jpg',
            },
            {
              id: 2,
              label: 'wheels',
              title: '17-Inch Black Alloy Wheels w/ Tires',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-17in-black-wheel-thumb-320-1x.jpg',
            },
            {
              id: 3,
              label: 'wheels',
              title: '17-Inch Machine-Finished Alloy Wheels w/Tires',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-17in-machine-finished-black-wheel-thumb-320-1x.jpg',
            },
          ],
        },
      ],
    },
    {
      title: 'packages',
      subCategories: [
        {
          title: 'adventure',
          options: [
            {
              title: 'Honda Performance Package 1',
              optionDescription: 'Lorem ipsum dolor sit amet.',
              price: 100,
            },
            {
              title: 'Honda Performance Package 2',
              optionDescription: 'Lorem ipsum dolor sit amet.',
              price: 200,
            },
            {
              title: 'Honda Performance Package 3',
              optionDescription: 'Lorem ipsum dolor sit amet.',
              price: 300,
            },
          ],
        },
        {
          title: 'urban',
          options: [
            {
              title: 'Honda Performance Package 1',
              optionDescription: 'Lorem ipsum dolor sit amet.',
              price: 100,
            },
            {
              title: 'Honda Performance Package 2',
              optionDescription: 'Lorem ipsum dolor sit amet.',
              price: 200,
            },
            {
              title: 'Honda Performance Package 3',
              optionDescription: 'Lorem ipsum dolor sit amet.',
              price: 300,
            },
            {
              title: 'Honda Performance Package 4',
              optionDescription: 'Lorem ipsum dolor sit amet.',
              price: 400,
            },
          ],
        },
      ],
    },
    {
      title: 'accessories',
      subCategories: [
        {
          title: 'exterior',
          options: [
            {
              id: 1,
              label: 'exterior',
              title: 'Bodyside Molding',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-body-side-molding-320-1x.jpg',
            },
            {
              id: 2,
              label: 'exterior',
              title: 'Decklid Spoiler',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-decklid-spoiler-320-1x.jpg',
            },
            {
              id: 3,
              label: 'exterior',
              title: 'Door Visors',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-door-visors-320-1x.jpg',
            },
          ],
        },
        {
          title: 'interior',
          options: [
            {
              id: 1,
              label: 'interior',
              title: 'All-Season Floor Mats',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2017/Civic-Sedan/Build-and-Price/Thumbnails/2016-civic-sedan-touring-acc-all-season-floor-mats-thumb-320-2x.jpg',
            },
            {
              id: 2,
              label: 'interior',
              title: 'Armrest Illumination - Blue',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-blue-armrest-illumination-320-1x.jpg/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-blue-armrest-illumination-320-1x.jpg',
            },
            {
              id: 3,
              label: 'interior',
              title: 'Cargo Hook',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-cargo-hooks-thumb-320-1x.jpg',
            },
          ],
        },
        {
          title: 'electronic',
          options: [
            {
              id: 1,
              label: 'electronic',
              title: 'Electronic sit amet 1',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2017/Civic-Sedan/Build-and-Price/Thumbnails/2016-civic-sedan-touring-acc-all-season-floor-mats-thumb-320-2x.jpg',
            },
            {
              id: 2,
              label: 'electronic',
              title: 'Electronic sit amet 2',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-blue-armrest-illumination-320-1x.jpg/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-blue-armrest-illumination-320-1x.jpg',
            },
            {
              id: 3,
              label: 'electronic',
              title: 'Electronic sit amet 3',
              imageUrl:
                'http://blue-qa.rpa.com/-/media/Honda-Automobiles/Vehicles/2019/Civic-Sedan/Build-and-Price/Thumbnails/MY19-civic-sedan-cargo-hooks-thumb-320-1x.jpg',
            },
          ],
        },
      ],
    },
  ],
}

export default trimGroupConfig
