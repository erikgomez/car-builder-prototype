import React from 'react'
import { useHistory } from 'react-router-dom'
import { NavContext } from '../context/NavContext'

const useBAPNav = () => {
  let history = useHistory()
  const [navState, setNavState] = React.useContext(NavContext)

  const goToIndex = index => {
    let tgtIndex =
      index < 0
        ? 0
        : index > navState.navLinks.length - 1
        ? navState.navLinks.length - 1
        : index
    setNavState(navState => ({ ...navState, curNavIndex: tgtIndex }))
    history.push(`/trim/${navState.navLinks[tgtIndex]}`)
  }

  const goNext = () => {
    goToIndex(navState.curNavIndex + 1)
  }

  const goPrev = () => {
    goToIndex(navState.curNavIndex - 1)
  }

  const goToRoute = route => {
    goToIndex(navState.navMap[route])
  }

  return {
    goNext,
    goPrev,
    goToIndex,
    goToRoute,
    curNavIndex: navState.curNavIndex,
    navLinks: navState.navLinks,
    navMap: navState.navMap,
  }
}

export default useBAPNav
