import React from 'react'

import { JellyContext } from '../context/JellyContextProvider'

const useJellyToggle = () => {
  const [jellyVisibility, setJellyVisibility] = React.useContext(JellyContext)

  const toggleJelly = () => {
    setJellyVisibility(!jellyVisibility)
  }

  return {
    toggleJelly,
  }
}

export default useJellyToggle
