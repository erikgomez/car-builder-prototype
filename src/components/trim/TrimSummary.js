import React from 'react'
import JellyBean from './JellyBean'
import { Link } from 'react-router-dom'

const TrimSummary = () => {
  return (
    <>
      <h5 className='text-secondary ml-4' style={{ fontWeight: 900 }}>
        {' '}
        SUMMARY
      </h5>
      <JellyBean
        vehicleTitle={'Civic Sedan Sport'}
        vehicleYear={'2019'}
        vehicleImage={'http://fpoimg.com/300x200?text=Sport&text_color=ffffff'}
        isVisible={true}
      />
      <ul className='nav nav-tabs justify-content-center'>
        <li className='nav-item'>
          <a className='nav-link btn-light active' href='#1'>
            LEASE
          </a>
        </li>
        <li className='nav-item'>
          <a className='nav-link btn-secondary' href='#1'>
            FINANCE
          </a>
        </li>
      </ul>

      <hr />
      <Link to='/trim/powertrain' className='btn btn-light' role='button'>
        Powertrain
      </Link>

      <hr />
      <Link to='/trim/appearance' className='btn btn-light' role='button'>
        Appearance
      </Link>
      <hr />
      <Link to='/trim/packages' className='btn btn-light' role='button'>
        Packages
      </Link>
      <hr />
      <Link to='/trim/accessories' className='btn btn-light' role='button'>
        Accesories
      </Link>
      <hr />
    </>
  )
}

export default TrimSummary
