import React from 'react'
import useBAPNav from '../../hooks/useBAPNav'
import cx from 'classnames'

const PrevNextNav = () => {
  const { goNext, goPrev, curNavIndex, navLinks } = useBAPNav()

  const handleNextClick = e => {
    goNext()
  }

  const handlePrevClick = e => {
    goPrev()
  }

  return (
    <div
      className={cx(
        'prev-next-container w-100 justify-content-between text-right px-3',
        { 'd-flex': curNavIndex > 0 }
      )}
    >
      {curNavIndex > 0 && (
        <button className="" onClick={handlePrevClick}>
          {`< PREV`}
        </button>
      )}
      {/* {curNavIndex} */}
      <button className="" onClick={handleNextClick}>
        {curNavIndex === navLinks.length - 1
          ? ''
          : curNavIndex === navLinks.length - 2
          ? 'Summary >'
          : 'NEXT >'}
      </button>
    </div>
  )
}

export default PrevNextNav
