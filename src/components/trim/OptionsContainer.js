import React, { useContext, useRef, useEffect, useState } from 'react'
import cx from 'classnames'

import { JellyContext } from '../../context/JellyContextProvider'

import Option from './Option'

const OptionsContainer = ({ options, hasHeadline, layoutType }) => {
  const [jellyVisibility] = useContext(JellyContext)
  const [xAxis, setXAxis] = useState(0)
  const optionsPanelRef = useRef(document.querySelectorAll('.option-card')[0])

  // onUpdate...
  useEffect(() => {
    if (optionsPanelRef.current) optionsPanelRef.current.scrollLeft = xAxis
  }, [xAxis])

  return (
    <div className="options-container">
      <div className="options-wrap" ref={optionsPanelRef}>
        <div
          className={cx('inner-options-wrap', {
            'expanded-panel': !jellyVisibility,
          })}
        >
          {hasHeadline && jellyVisibility && (
            <h4 className="options-headline">COLOR NAME</h4>
          )}
          <ul className={layoutType ? layoutType : ''}>
            {options.map((option, key) => {
              return (
                <Option
                  key={key}
                  options={options}
                  setXAxis={setXAxis}
                  label={option.label}
                  isExpandable={layoutType === 'expandable'}
                />
              )
            })}
          </ul>
        </div>
      </div>
    </div>
  )
}

export default OptionsContainer
