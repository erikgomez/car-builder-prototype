import React from 'react'
import { Link } from 'react-router-dom'
import cx from 'classnames'
import useBAPNav from '../../hooks/useBAPNav'

import BAPConfigContext from '../../context/BAPConfigContext'
import { JellyContext } from '../../context/JellyContextProvider'

const CategoryNav = ({ category }) => {
  const [jellyVisibility] = React.useContext(JellyContext)
  const { categories } = React.useContext(BAPConfigContext)
  const { goToRoute } = useBAPNav()

  React.useEffect(() => {
    console.log('CategoryNav')
    // document.querySelector('.category-nav li .active').scrollIntoView()
  })

  return (
    <ul
      className={cx(
        'category-nav',
        { 'fade-down-in': jellyVisibility },
        { 'fade-up-out': !jellyVisibility }
      )}
    >
      {categories.map((item, index) => {
        let linkRef = React.createRef()

        const handleClick = event => {
          let currentActive = document.querySelector('.category-nav .active')
          currentActive.classList.remove('active')
          event.currentTarget.classList.add('active')
          console.log(linkRef.current.offsetLeft)
          console.log(linkRef.current.getBoundingClientRect())
          // linkRef.current.scrollIntoView()
          event.currentTarget.scrollIntoView()

          // TODO: either we use Link to navigate and update state
          // or use goToRoute as a side effect. This is not good.
          goToRoute(item.title)
        }

        return (
          <li key={index} ref={linkRef}>
            <Link
              to={`/trim/${item.title}`}
              className={cx({ active: item.title === category }, '')}
              onClick={e => {
                handleClick(e)
              }}
            >
              {item.title}
            </Link>
          </li>
        )
      })}
    </ul>
  )
}

export default CategoryNav
