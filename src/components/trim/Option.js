import React, { useState, useRef, useEffect, useContext } from 'react'

import { JellyContext } from '../../context/JellyContextProvider'

import useJellyToggle from '../../hooks/useJellyToggle'

const Option = ({ setXAxis, label, isExpandable }) => {
  const [jellyVisibility] = useContext(JellyContext)

  const [showCutlineText, setShowCutlineText] = useState(false)
  const [isExpanded, setIsExpanded] = useState(jellyVisibility)

  const isInitialMount = useRef(true)
  const targetOffset = useRef(null)

  const { toggleJelly } = useJellyToggle()

  const isIncluded = true // TODO: figure out a way to set this dynamically
  const transitionDuration = 300

  useEffect(() => {
    // run useEffect only on updates except initial mount
    if (isInitialMount.current) {
      isInitialMount.current = false
    } else {
      setTimeout(() => {
        setXAxis(targetOffset.current.offsetLeft)
      }, transitionDuration)
    }
  }, [isExpanded, setXAxis])

  const toggleCutlineDisplay = () => {
    setShowCutlineText(!showCutlineText)
  }

  const handleOptionToggle = event => {
    toggleJelly()
    targetOffset.current = event.currentTarget.parentNode
    setIsExpanded(!isExpanded)
  }

  return (
    <li className="option">
      {jellyVisibility ? (
        <>
          {isExpandable && (
            <button className="expand-option-btn" onClick={handleOptionToggle}>
              DETAILS
            </button>
          )}
          <span>{label}</span>
        </>
      ) : (
        <>
          <button className="collapse-option-btn" onClick={handleOptionToggle}>
            {closeSVG()}
          </button>
          <div className="card-image" onClick={toggleCutlineDisplay}>
            {showCutlineText && (
              <p className="cutline-text">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                Laboriosam atque, quasi optio in voluptatem commodi sit,
                eveniet, fugit hic impedit quos minus!
              </p>
            )}
          </div>
          <div className="card-content-container">
            <h4 className="card-headline">18-inch-Aloy Wheels</h4>
            <p className="card-copy">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Obcaecati
              quod unde soluta, amet possimus ratione nihil excepturi
              dignissimos. Eveniet adipisci neque nemo eligendi aperiam?
            </p>
          </div>
          <div className="option-availability">
            <div className="seperator"></div>
            {isIncluded ? (
              <p>INCLUDED</p>
            ) : (
              <>
                <p className="option-price">$2,265</p>
                <button className="option-toggle">ADD</button>
              </>
            )}
          </div>
        </>
      )}
    </li>
  )
}

const closeSVG = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 49.996 50">
      <path d="M25 2c12.682 0 23 10.318 23 23S37.682 48 25 48 2 37.682 2 25 12.318 2 25 2m0-2C11.193 0 0 11.193 0 25s11.193 25 25 25 25-11.193 25-25S38.807 0 25 0z" />
      <path d="M15.2 17.045l1.415-1.414 18 18-1.414 1.414z" />
      <path d="M15.2 33.63l18-17.999 1.415 1.414-18 18z" />
    </svg>
  )
}

export default Option
