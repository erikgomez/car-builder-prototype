import React, { useContext } from 'react'
import { Link } from 'react-router-dom'
import cx from 'classnames'

import { JellyContext } from '../../context/JellyContextProvider'

import useBAPNav from '../../hooks/useBAPNav'

const SubCategoryNav = ({ subCategories, match }) => {
  const { goToRoute } = useBAPNav()
  const [jellyVisibility] = useContext(JellyContext)

  if (subCategories.length <= 1 || subCategories[0].title === '') {
    return null
  } else {
    return (
      <div
        className={cx(
          'trim-sub-nav',
          { 'fade-down-in': jellyVisibility },
          { 'fade-up-out': !jellyVisibility }
        )}
      >
        <ul>
          {subCategories.map((subCat, i) => {
            return (
              <li key={`sub${i}`}>
                <Link
                  to={`/trim/${match.params.category}/${subCat.title}`}
                  className={cx({
                    active: subCat.title === match.params.subcategory,
                  })}
                  onClick={e => {
                    const route = `${match.params.category}/${subCat.title}`
                    // TODO: either we use Link to navigate and update state
                    // or use goToRoute as a side effect. This is not good.
                    goToRoute(route)
                  }}
                >
                  {subCat.title} ({subCat.options.length})
                </Link>
              </li>
            )
          })}
        </ul>
      </div>
    )
  }
}

export default SubCategoryNav
