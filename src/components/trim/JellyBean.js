import React, { useContext } from 'react'
import cx from 'classnames'

import { JellyContext } from '../../context/JellyContextProvider'

const JellyBean = ({ vehicleTitle, vehicleYear }) => {
  const [jellyVisibility] = useContext(JellyContext)
  return (
    <div
      className={cx(
        'jellybean',
        { 'animate-up-fade-out': !jellyVisibility },
        { 'animate-down-fade-in': jellyVisibility }
      )}
    >
      <div className="car-image"></div>
      <h3 className="text-center w-100">
        {vehicleYear} {vehicleTitle}
      </h3>
    </div>
  )
}

export default JellyBean
