import React, { useContext } from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import { JellyContext } from '../../context/JellyContextProvider'

import JellyBean from './JellyBean'
import CategoryNav from './CategoryNav'
import CategoryPanel from './CategoryPanel'
import PrevNextNav from './PrevNextNav'
import TrimSummary from './TrimSummary'

const TrimPanel = () => {
  const [jellyVisibility] = useContext(JellyContext)

  return (
    <>
      <Switch>
        <Redirect exact from="/trim" to="/trim/powertrain" />
        <Route
          path="/trim/:category"
          render={({ match }) => {
            return (
              <>
                {match.params.category === 'summary' ? (
                  <TrimSummary />
                ) : (
                  <div>
                    <CategoryNav category={match.params.category} />
                    <JellyBean
                      vehicleTitle={'Civic Sedan Sport'}
                      vehicleYear={'2019'}
                      vehicleImage={
                        'http://fpoimg.com/300x200?text=Sport&text_color=ffffff'
                      }
                    />
                    <CategoryPanel match={match} />
                  </div>
                )}
              </>
            )
          }}
        />
      </Switch>
      {jellyVisibility && <PrevNextNav />}
    </>
  )
}

export default TrimPanel
