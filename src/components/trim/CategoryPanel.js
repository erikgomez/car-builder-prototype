import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'

import BAPConfigContext from '../../context/BAPConfigContext'

import SubCategoryNav from './SubCategoryNav'
import OptionsContainer from './OptionsContainer'

const CategoryPanel = ({ match }) => {
  const data = React.useContext(BAPConfigContext)
  const { subCategories } = data.categories.find(
    cat => cat.title === match.params.category
  )

  return (
    <Switch>
      {subCategories.length >= 1 && subCategories[0].title !== '' && (
        <Redirect
          exact
          from={`${match.path}`}
          to={`${match.path}/${subCategories[0].title}`}
        />
      )}
      <Route
        path={[`${match.path}/:subcategory`, `${match.path}`]}
        render={({ match }) => {
          const { options } = match.params.subcategory
            ? subCategories.find(cat => cat.title === match.params.subcategory)
            : subCategories[0]
          const layoutType =
            (options[0].label === 'interior' && 'swatch') ||
            (options[0].label === 'exterior' && 'swatch') ||
            (options[0].label === 'wheels' && 'expandable') ||
            (options[0].label === 'electronic' && 'expandable') ||
            'expandable'

          const hasHeadline =
            options[0].label === 'interior' || options[0].label === 'exterior'

          return (
            <>
              <SubCategoryNav subCategories={subCategories} match={match} />

              <OptionsContainer
                options={options}
                hasHeadline={hasHeadline}
                layoutType={layoutType}
              />
            </>
          )
        }}
      />
    </Switch>
  )
}

export default CategoryPanel
