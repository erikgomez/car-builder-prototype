import React from 'react'
import { Link } from 'react-router-dom'

const TrimHeader = () => {
  return (
    <div className='secondary-nav'>
      <Link className='link' to='/'>
        Change Your Trim
      </Link>

      <Link className='link' to='/trim/summary'>
        <div className='summary-text'>
          <p className='msrp'>$XX,XXX</p>
          <p className='summary'>SUMMARY</p>
        </div>
      </Link>
      <hr />
    </div>
  )
}

export default TrimHeader
