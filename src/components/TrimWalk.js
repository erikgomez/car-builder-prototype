/**
 * This interim component is a placeholder for the actual Trim Walk component
 * that triggers the BAP component which is the star of this prototype.
 *
 */

import React from 'react'
import { Link } from 'react-router-dom'
import JellyBean from './trim/JellyBean'

const TrimWalk = () => {
  return (
    <div className="build-and-price d-flex flex-column ">
      <div className="secondary-nav">
        <a className="link" href="#top">
          {`< Change Vehicle`}
        </a>
        <hr />
      </div>

      <div className="tw-header-container mt-2">
        <h5 className="text-secondary">BUILD & PRICE</h5>
        <h2 className="text-secondary">Select Your Trim</h2>
      </div>

      <div className="tw-content-container border border-secondary mt-2 mx-3 py-2">
        <JellyBean vehicleTitle={'Civic Sedan Sport'} vehicleYear={'2018'} />
        <div className="tw-cta-wrap">
          <Link
            to="/trim"
            className="btn btn-secondary btn-lg mt-4"
            role="button"
          >
            BUILD
          </Link>
        </div>
      </div>
    </div>
  )
}

export default TrimWalk
