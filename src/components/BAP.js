import React from 'react'
import TrimHeader from './trim/TrimHeader'
import TrimPanel from './trim/TrimPanel'
import { NavContextProvider } from '../context/NavContext'
import { JellyContextProvider } from '../context/JellyContextProvider'

const BAP = () => {
  return (
    <div className="build-and-price">
      <NavContextProvider>
        <TrimHeader />
        <JellyContextProvider>
          <TrimPanel />
        </JellyContextProvider>
      </NavContextProvider>
    </div>
  )
}

export default BAP
