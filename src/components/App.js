import React from 'react'
import { HashRouter as Router, Route, Redirect, Switch } from 'react-router-dom'
import BAP from './BAP'
import TrimWalk from './TrimWalk'
import MenuSort from './MenuSort'
import '../App.scss'

/**
 * For the purpose of the prototype this App component
 * serves as the entry point to instantiate BAP which is the
 * main component enclosing the Build and Price tool functionality.
 *
 * A couple of auxiliary components (MenuSort and TrimWalk) are in place
 * just as context (the actual production ones should run their own
 * dev cycle / React componentization process) and should only be taken into
 * account here as a reference as to how to interact with the BAP component.
 */

const App = () => {
  return (
    <>
      <Router>
        <Switch>
          <Route path="/menu" render={() => <MenuSort />} />
          <Route path="/trim" render={() => <BAP />} />
          <Route path="/trim-walk" render={() => <TrimWalk />} />
          <Redirect from="/" to="/trim-walk" />
        </Switch>
      </Router>
    </>
  )
}

export default App
